/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var mongoose = require('mongoose');

var uristring = 'mongodb://localhost/durBusDb';

// Makes connection asynchronously. Mongoose will queue up database
// operations and release them when the connection is complete.
mongoose.connect(uristring, function (err, res) {
  if (err) {
    console.log ('ERROR connecting to: ' + uristring + '. ' + err);
  } else {
    console.log ('Succeeded connected to: ' + uristring);
  }
});

/**
 * Schema definitions.
 */

mongoose.model('OAuthTokens', new Schema({
  accessToken: { type: String },
  accessTokenExpiresOn: { type: Date },
  clientId: { type: String },
  refreshToken: { type: String },
  refreshTokenExpiresOn: { type: Date },
  userId: { type: String }
}));

mongoose.model('OAuthClients', new Schema({
  clientId: { type: String },
  clientSecret: { type: String },
  redirectUris: { type: Array }
}));

mongoose.model('OAuthUsers', new Schema({
  email: { type: String, default: '' },
  firstname: { type: String },
  lastname: { type: String },
  password: { type: String },
  username: { type: String }
}));

var OAuthTokensModel = mongoose.model('OAuthTokens');
var OAuthClientsModel = mongoose.model('OAuthClients');
var OAuthUsersModel = mongoose.model('OAuthUsers');

var a = new OAuthUsersModel({
  email: 'info@linkle.net',
  firstname: 'tahsin',
  lastname: 'yüksel',
  password: '123',
  username: 'tahsin'
});

a = new OAuthClientsModel({
  clientId: '19920913',
  clientSecret: '01012017',
  redirectUris: 'giris'
});

//a.save();

/**
 * Get access token.
 */

module.exports.getAccessToken = function(bearerToken) {
  console.log('in getAccessToken (bearerToken: ' + bearerToken + ')');

  var a = OAuthTokensModel.findOne({ accessToken: bearerToken });
  //console.log(a);
  return a;
};

/**
 * Get client.
 */

module.exports.getClient = function(clientId, clientSecret) {
  console.log('in getClient (clientId: ' + clientId + ', clientSecret: ' + clientSecret + ')');

  return OAuthClientsModel.findOne({ clientId: clientId, clientSecret: clientSecret });
};

/**
 * Get refresh token.
 */

module.exports.getRefreshToken = function(refreshToken) {
  console.log('in getRefreshToken (refreshToken: ' + refreshToken + ')');

  return OAuthTokensModel.findOne({ refreshToken: refreshToken });
};

/*
 * Get user.
 */

module.exports.getUser = function(username, password) {
  console.log('in getUser (username: ' + username + ', password: ' + password + ')');

  return OAuthUsersModel.findOne({ username: username, password: password });
};

/**
 * Save token.
 */

module.exports.saveToken = function(token, client, user) {
  console.log('in saveToken (token: ' + token + ')');

  var accessToken = new OAuthTokensModel({
    accessToken: token.accessToken,
    accessTokenExpiresOn: token.accessTokenExpiresOn,
    clientId: client.id,
    refreshToken: token.refreshToken,
    refreshTokenExpiresOn: token.refreshTokenExpiresOn,
    userId: user.id
  });

  return accessToken.save();
};