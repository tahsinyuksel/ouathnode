# Oauth2 NodeJs Base Integration
Understand the structure of the oauth2 and nodejs. Base and fast integration and examples.

## Install
```sh
$ cd ouathnode
$ npm install
$ node server.js
```

## Example Requests
Example requests and response for Oauth2

### Token Example Request:
Get access token. Input user name and password, return access token.
Client apps(web, mobile etc.) save to token. Because; To be used in other requests.

Host url: localhost:3000/oauth/token
Header: Authorization ("Basic " + base64(client_id:client_secret) ),
Body: grant_type ("password"), username and password in the request body

`Use Code`:
```sh
  app.all('/oauth/token', function(req,res,next){
    var request = new Request(req);
    var response = new Response(res);

    oauth
      .token(request,response)
      .then(function(token) {
        // Todo: remove unnecessary values in response
        return res.json(token)
      }).catch(function(err){
        return res.status( 500).json(err)
      })
  });
```

`Token For example`
POST /oauth/token HTTP/1.1
Host: server.example.com
Authorization: Basic ZGVtb2NsaWVudDpkZW1vY2xpZW50c2VjcmV0
Content-Type: application/x-www-form-urlencoded

grant_type=password&username=admin&password=admin&scope=profile

Example Response:
```sh
{
  "client": {
    "_id": "5889a5d0f8487429186cddfd",
    "client_id": "democlient",
    "client_secret": "democlientsecret",
    "redirect_uri": "http://localhost/cb",
    "User": "5889a5d0f8487429186cddf6",
    "scope": "profile",
    "__v": 0
  },
  "user": {
    "_id": "5889a5d0f8487429186cddf6",
    "username": "admin",
    "password": "admin",
    "scope": "profile",
    "__v": 0
  },
  "access_token": "672dd036ca4fb4fa04d51ffe7947a91db51b24c0",
  "refresh_token": "b0586f328ecfda229959db4e1b826d37b58f4a3d",
  "accessToken": "672dd036ca4fb4fa04d51ffe7947a91db51b24c0",
  "accessTokenExpiresAt": "2017-01-26T20:11:29.742Z",
  "refreshToken": "b0586f328ecfda229959db4e1b826d37b58f4a3d",
  "refreshTokenExpiresAt": "2017-02-09T19:11:29.742Z",
  "scope": true
}
```

### Authenticate Filter For Request
`Use Code`:
```sh
app.get('/me', authenticate(), function(req,res){
  res.json({
    me: req.user,
    messsage: 'Authorization success, Without Scopes, Try accessing /profile with `profile` scope',
    description: 'detail me',
    more: 'pass `profile` scope while Authorize'
  })
});
```

Host url: localhost:3000/me
Header: Authorization Bearer 0143148c242cce70c3bb7e4ebeceefd5570973c8

Example Response:
```sh
{
  "me": {
    "_id": "588a3b1004b4c4606c4889bc",
    "access_token": "0143148c242cce70c3bb7e4ebeceefd5570973c8",
    "expires": "2017-01-26T19:08:16.988Z",
    "OAuthClient": {
      "_id": "5889a5d0f8487429186cddfd",
      "client_id": "democlient",
      "client_secret": "democlientsecret",
      "redirect_uri": "http://localhost/cb",
      "User": "5889a5d0f8487429186cddf6",
      "scope": "profile",
      "__v": 0
    },
    "User": {
      "_id": "5889a5d0f8487429186cddf6",
      "username": "admin",
      "password": "admin",
      "scope": "profile",
      "__v": 0
    },
    "scope": "true",
    "__v": 0
  },
  "messsage": "Authorization success, Without Scopes, Try accessing /profile with `profile` scope",
  "description": "detail me",
  "more": "pass `profile` scope while Authorize"
}
```

### Authenticate `Scope` For Request
`Use Code`:
```sh
app.get('/profile', authenticate({scope:'profile'}), function(req,res){
  res.json({
    profile: req.user
  })
});
```

Host Url: localhost:3000/profile
Header: Authorization Bearer 0143148c242cce70c3bb7e4ebeceefd5570973c8

Example Response:
```sh
{
  "profile": {
    "_id": "588a3b1004b4c4606c4889bc",
    "access_token": "0143148c242cce70c3bb7e4ebeceefd5570973c8",
    "expires": "2017-01-26T19:08:16.988Z",
    "OAuthClient": {
      "_id": "5889a5d0f8487429186cddfd",
      "client_id": "democlient",
      "client_secret": "democlientsecret",
      "redirect_uri": "http://localhost/cb",
      "User": "5889a5d0f8487429186cddf6",
      "scope": "profile",
      "__v": 0
    },
    "User": {
      "_id": "5889a5d0f8487429186cddf6",
      "username": "admin",
      "password": "admin",
      "scope": "profile",
      "__v": 0
    },
    "scope": "true",
    "__v": 0
  }
}
```

### Installation

Requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies and start the server.

### Tech And Requirements

Uses a projects to dependencies:

* [node-oauth2-server](https://www.npmjs.com/package/node-oauth2-server) - (oauth2-server": "^3.0.0-b2)
* [Mongoose](https://www.npmjs.com/package/mongoose) - (oauth2-server": "^3.0.0-b2)
* Body-parser
* Express
* Look at the package.json

## Edit - Fixed
Default model: mongo-models.js

- validateScope
 mongo-models.js in "validateScope" edited:
 change validateScope(token, scope) to validateScope(user, client, scope)

- verifyScope
add verifyScope(accessToken) in mongo-models.js.
This will be filled.

- Default Db
Default db - store mongodb. Config sets. Models in db.

## Todos

 - Write Tests
 - Demo live application
 - Store adapters for memory stores (redis, memcached), dbs (mysql, pqsl)
 - Socket.io oauth
 - Passport support

## Resource And Referances
https://github.com/oauthjs/node-oauth2-server
https://github.com/manjeshpv/node-oauth2-server-implementation

## Contact
Questions, suggestions, comments:
Tahsin Yüksel info@tahsinyuksel.com

License
----

MIT
